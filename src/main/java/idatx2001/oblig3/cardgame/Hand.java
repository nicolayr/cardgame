package idatx2001.oblig3.cardgame;

import java.util.ArrayList;

/**
 * Represent a hand dealt by the dealHand(int n)-method in the DeckOfCards-class.
 * Allows for checking of hand. This can be used when creating different games with the deck of cards.
 */
public class Hand {

    // Creates list of cards that will represents the hand.
    private final ArrayList<PlayingCard> hand;

    /**
     * Constructor that creates the Hand
     * @param hand list of cards
     */
    public Hand(ArrayList<PlayingCard> hand) {
        this.hand = hand;
    }

    /**
     * Returns the list of cards that is in the hand.
     * @return hand
     */
    public ArrayList<PlayingCard> getHand() {
        return hand;
    }

    /**
     * Retrieves a card at the position in the deck.
     * @param index position in deck.
     * @return card at position.
     */
    public PlayingCard getCardAt(int index) {
        return hand.get(index);
    }

    // Now comes all the rules for this particular game. Notice that all my return-types are of type String.
    // I originally created the methods with more natural return-types, like boolean, ArrayLists and so on,
    // but because of what they would be used for it just made everything easier to return strings,
    // so i changed it. Alternatively i could've just handled all of this in the controller, which might have made for
    // more correct code.



    /**
     * Checks if the hand is all of one suit, also known as it being "Flush".
     * @return "Yes" if hand is flush, "No" if not.
     */
    public String checkFlush() {
        char suit = hand.get(0).getSuit();
        for(PlayingCard c : hand) {
            if(c.getSuit() != suit) {
                return "No";
            }
        }
        return "Yes";
    }

    /**
     * Uses streams and anyMatch to check if the hand is all made up of one suit, also known as a "Flush".
     * @return "Yes" if hand is flush, "No" if not.
     */
    public String checkFlushStreams() {
        if(hand.stream().anyMatch(p -> p.getSuit() != hand.get(0).getSuit())) {
            return "No";
        }
        return "Yes";
    }

    /**
     * Calculates the sum of all the faces in the hand. Aces are valued at 1.
     * @return sum of all faces in hand
     */
    public String sumOfFaces() {
        int sum = 0;
        for(PlayingCard c : hand) {
            sum += c.getFace();
        }
        return String.valueOf(sum);
    }

    /**
     * Streams-version of sumOfFaces()
     * @return sum of all faces in hand
     */
    public String sumOfFacesStreams() {
        return String.valueOf(hand.stream().map(PlayingCard::getFace).reduce(Integer::sum).get());
    }

    /**
     * Goes through all cards in the hand and checks if it is hearts. If it is, it will be added to
     * an ArrayList. This list gets returned. If there are no hearts, the list will be empty.
     * @return ArrayList of hearts in the hand.
     */
    public String getHearts() {
        StringBuilder hearts = new StringBuilder();
        for(PlayingCard c : hand) {
            if(c.getSuit() == 'H') {
                hearts.append(c.getAsString()).append(", ");
            }
        }
        if(hearts.toString().equals("")) {
            return "No hearts";
        }
        return hearts.toString();
    }

    /**
     * Streams-version of getHeartsStreams()
     * @return sum of all faces in hand
     */
    public String getHeartsStreams() {
        StringBuilder hearts = new StringBuilder();
        hand.stream().filter(p -> p.getSuit() == 'H').forEach(p -> hearts.append(p.getAsString()).append(", "));
        if(hearts.toString().equals("")) {
            return "No hearts";
        }
        return hearts.deleteCharAt(hearts.length() -1).deleteCharAt(hearts.length() -1).toString();
    }

    /**
     * Checks if the hand has a Queen of Spades.
     * @return Yes if hand has Queen of Spades, no if not.
     */
    public String checkQueenOfSpades() {
        for (PlayingCard c : hand) {
            if(c.getAsString().equals("S12")) {
                return "Yes";
            }
        }
        return "No";
    }

    /**
     * Streams-version of checkQueenOfSpades()
     * @return "Yes" if hand has Queen of Spades, "No" if not.
     */
    public String checkQueenOfSpadesStreams() {
        if(hand.stream().anyMatch(playingCard -> playingCard.getAsString().equals("S12"))) {
            return "Yes";
        }
        return "No";
    }


    /**
     * Allows for a hand to be represented as a humanly readable string.
     * @return formatted, human-readable version of a hand.
     */
    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("-----Hand-----\n");
        for (PlayingCard c : hand) {
            string.append(c.getAsString()).append("\n");
        }
        return string.toString();
    }

}
