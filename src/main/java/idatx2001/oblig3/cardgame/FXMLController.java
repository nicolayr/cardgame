package idatx2001.oblig3.cardgame;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class FXMLController {

    // Creating deck
    private final DeckOfCards deck = new DeckOfCards();
    private Hand hand;
    // Displaying the cards
    public ImageView card0;
    public ImageView card1;
    public ImageView card2;
    public ImageView card3;
    public ImageView card4;

    // Labels for the different tasks
    public Label sumOfFaces;
    public Label getHearts;
    public Label isFlush;
    public Label hasQueenOfSpades;

    /**
     * Deals a new hand, displays images representing the playing cards in the hand, and calculates all the
     * wanted values from the task given.
     */
    public void onDealHandButtonClick(){
        // Dealing hand
        dealHand();

        // Displaying cards
        try {
            displayCards();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Updating all the numbers (In hindsight, keeping original return-types and creating methods in here
        // that would translate the data in to string and call them in this method would make a lot more sense
        // seeing as this is OO-programming. In interest of time i will not change this back.
        sumOfFaces.setText(hand.sumOfFacesStreams());
        getHearts.setText(hand.getHeartsStreams());
        isFlush.setText(hand.checkFlushStreams());
        hasQueenOfSpades.setText(hand.checkQueenOfSpadesStreams());
    }

    /**
     * "Deals" five new cards.
     */
    public void dealHand() {
        this.hand = new Hand(deck.dealHand(5));
        System.out.println("Hand dealt");
    }

    /**
     * Changes the images on the "board" to represent the cards in the Hand.
     * @throws FileNotFoundException if the program can't find the files.
     */
    public void displayCards() throws FileNotFoundException{

        card0.setImage(new Image(this.getClass().getResourceAsStream("/playingCards/" +
                hand.getCardAt(0).getAsString() + ".png")));

        card1.setImage(new Image(this.getClass().getResourceAsStream("/playingCards/" +
                hand.getCardAt(1).getAsString() + ".png")));

        card2.setImage(new Image(this.getClass().getResourceAsStream("/playingCards/" +
                hand.getCardAt(2).getAsString() + ".png")));

        card3.setImage(new Image(this.getClass().getResourceAsStream("/playingCards/" +
                hand.getCardAt(3).getAsString() + ".png")));

        card4.setImage(new Image(this.getClass().getResourceAsStream("/playingCards/" +
                hand.getCardAt(4).getAsString() + ".png")));

    }
}
