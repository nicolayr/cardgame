package idatx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Random;

/**
 * A class representing a deck of 52 cards. The deck a fixed array of PlayingCards, where each position in the list
 * represents a card. The deck is in order from 1 through 13, suit by suit.
 * The class also includes a method which allows for "dealing of a hand",
 * which basically just generates a random list of the cards in the deck.
 */
public class DeckOfCards {

    /**
     * Fixed array with four characters, each representing their own suit in a deck of cards.
     * 'S': Spades
     * 'H': Hearts
     * 'D': Diamonds
     * 'C': Clubs
     */
    private final char[] suit = {'S','H','D', 'C'};
    private final PlayingCard[] deckOfCards = new PlayingCard[52];
    private Random random = new Random();

    /**
     * Constructs a complete deck of 52 playing cards.
     */
    public DeckOfCards() {
        int index = 0; // Position in deck
        for(char s : suit) {
            for (int i = 1; i <= 13; i++) {
                deckOfCards[index] = new PlayingCard(s, i); // Assigns a playing card to current index.
                // The playing card will have the current suit in the for-each-loop as well as the number
                // on the current cycle. It runs though all the suits and the finishes.
                index++;
            }
        }
    }

    /**
     * Retrieves the card at a given position as a string.
     * @param index position of playing card in the deck
     * @return a card as a string. The format is defined in the playing card class. Example: if the index is
     * 12, this method should return a string that says "S13", as that is the expected card in that position.
     */
    public String getCard(int index) {
        return deckOfCards[index].getAsString();
    }

    /**
     * Creates an arraylist of five random cards, where no cards is the same. This emulates that when a card is dealt
     * the card is no longer in the deck, thus all cards in the hand must be different.
     * @param n number of cards in the hand. N has to be between 1 and 52, since at least one
     *          card has to be dealt, and there only are 52 cards in the deck.
     * @return an arraylist og cards, representing a "hand".
     */
    public ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException{
        if(n < 1 || n > 52) {
            throw new IllegalArgumentException("Please choose a number between 1 and 52.");
        }
        ArrayList<PlayingCard> hand = new ArrayList<>();
        PlayingCard newCard;
        for(int i = 0; i < n; i++) {
            newCard = deckOfCards[random.nextInt(52)];
            if(hand.contains(newCard)) {
                i--;
            } else {
                hand.add(newCard);
            }
        }
        return hand;
    }

}
