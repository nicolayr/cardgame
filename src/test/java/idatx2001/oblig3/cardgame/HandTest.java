package idatx2001.oblig3.cardgame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class HandTest {

    DeckOfCards deck = new DeckOfCards();

    @Test
    public void sumOfHand() {
        PlayingCard c12 = new PlayingCard('c', 12);
        PlayingCard d11 = new PlayingCard('d', 11);
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(c12);
        cards.add(d11);
        Hand hand = new Hand(cards);
        assertEquals("23" ,hand.sumOfFacesStreams());
    }

    @Nested
    @DisplayName("Check that flush-test works as intended")
    class flushCheckTest {

        @Test
        @DisplayName("Hand is flush")
        public void flushHandTest() {
            PlayingCard h1 = new PlayingCard('h', 1);
            PlayingCard h2 = new PlayingCard('h', 2);
            PlayingCard h3 = new PlayingCard('h', 3);
            PlayingCard h4 = new PlayingCard('h', 4);
            PlayingCard h5 = new PlayingCard('h', 5);
            ArrayList<PlayingCard> flushHand = new ArrayList<>();
            flushHand.add(h1);
            flushHand.add(h2);
            flushHand.add(h3);
            flushHand.add(h4);
            flushHand.add(h5);
            // We do this manually to make sure the hand is as wanted.
            Hand hand = new Hand(flushHand);
            assertEquals("Yes", hand.checkFlushStreams());
        }

        @Test
        @DisplayName("Hand is not flush")
        public void nonFlushHandTest() {
            PlayingCard h1 = new PlayingCard('c', 1);
            PlayingCard h2 = new PlayingCard('d', 1);
            ArrayList<PlayingCard> nonFlushHand = new ArrayList<>();
            nonFlushHand.add(h1);
            nonFlushHand.add(h2);
            Hand hand = new Hand(nonFlushHand);
            assertEquals("No", hand.checkFlushStreams());
        }
    }

    @Nested
    @DisplayName("Sorting out hearts")
    class getHeartsTests {

        @Test
        @DisplayName("Get hearts from a hand with hearts")
        public void getHeartsFromHandWithHearts() {
            PlayingCard h1 = new PlayingCard('H', 1);
            PlayingCard h2 = new PlayingCard('H', 2);
            PlayingCard h3 = new PlayingCard('H', 3);
            PlayingCard h4 = new PlayingCard('H', 4);
            PlayingCard h5 = new PlayingCard('H', 5);
            ArrayList<PlayingCard> handOfHearts = new ArrayList<>();
            handOfHearts.add(h1);
            handOfHearts.add(h2);
            handOfHearts.add(h3);
            handOfHearts.add(h4);
            handOfHearts.add(h5);
            // We do this manually to make sure the hand is as wanted.
            Hand hand = new Hand(handOfHearts);
            assertEquals("H1, H2, H3, H4, H5", hand.getHeartsStreams());
        }

        @Test
        @DisplayName("Get hearts from a hand without hearts")
        public void getHeartsFromHandWithoutHearts() {
            PlayingCard h1 = new PlayingCard('C', 1);
            PlayingCard h2 = new PlayingCard('C', 2);
            PlayingCard h3 = new PlayingCard('C', 3);
            PlayingCard h4 = new PlayingCard('C', 4);
            PlayingCard h5 = new PlayingCard('C', 5);
            ArrayList<PlayingCard> handOfNonHearts = new ArrayList<>();
            handOfNonHearts.add(h1);
            handOfNonHearts.add(h2);
            handOfNonHearts.add(h3);
            handOfNonHearts.add(h4);
            handOfNonHearts.add(h5);
            // We do this manually to make sure the hand is as wanted.
            Hand hand = new Hand(handOfNonHearts);
            assertEquals("No hearts", hand.getHeartsStreams());
        }
    }

    @Nested
    @DisplayName("Check if hand has queen of spades")
    class QueenOfSpadesCheck {

        @Test
        @DisplayName("Hand doest not contain queen of spades")
        public void doesNotHaveQueenOfSpades() {
            PlayingCard h1 = new PlayingCard('C', 1);
            PlayingCard h2 = new PlayingCard('C', 2);
            PlayingCard h3 = new PlayingCard('C', 3);
            PlayingCard h4 = new PlayingCard('C', 4);
            PlayingCard h5 = new PlayingCard('C', 5);
            ArrayList<PlayingCard> noQOS = new ArrayList<>();
            noQOS.add(h1);
            noQOS.add(h2);
            noQOS.add(h3);
            noQOS.add(h4);
            noQOS.add(h5);
            // We do this manually to make sure the hand is as wanted.
            Hand hand = new Hand(noQOS);
            assertEquals("No", hand.checkQueenOfSpadesStreams());
        }

        @Test
        @DisplayName("Hand does contain queen of spades")
        public void doesHaveQueenOfSpades() {
            PlayingCard h1 = new PlayingCard('C', 1);
            PlayingCard h2 = new PlayingCard('C', 2);
            PlayingCard h3 = new PlayingCard('C', 3);
            PlayingCard h4 = new PlayingCard('S', 12);
            PlayingCard h5 = new PlayingCard('C', 5);
            ArrayList<PlayingCard> withQOS = new ArrayList<>();
            withQOS.add(h1);
            withQOS.add(h2);
            withQOS.add(h3);
            withQOS.add(h4);
            withQOS.add(h5);
            Hand hand = new Hand(withQOS);
            assertEquals("Yes", hand.checkQueenOfSpadesStreams());
        }
    }

}
