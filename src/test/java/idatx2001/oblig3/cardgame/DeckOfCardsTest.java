package idatx2001.oblig3.cardgame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {

    DeckOfCards deck = new DeckOfCards();

    @Nested
    @DisplayName("Creating the deck")
    public class createDeckTest {

        @Test
        @DisplayName("Ace of spades")
        public void aceOfSpades() {
            assertEquals("S1", deck.getCard(0));
        }

        @Test
        @DisplayName("Ace of Hearts")
        public void aceOfHearts() {
            assertEquals("H1", deck.getCard(13));
        }

        @Test
        @DisplayName("King of Clubs")
        public void kingOfClubs() {
            assertEquals("C13", deck.getCard(51));
        }

    }

    @Nested
    @DisplayName("Dealing a hand")
    public class dealingHand {

        @Test
        @DisplayName("Dealing a hand of five")
        public void handOfFive() {
            ArrayList<PlayingCard> hand = deck.dealHand(5);
            assertEquals(5, hand.size());
        }

        @Test
        @DisplayName("Dealing a hand of -1")
        public void handOfMinusOne() {
            boolean handIsDealt;
            try {
                ArrayList<PlayingCard> hand = deck.dealHand(-1);
                handIsDealt = true;
            } catch (IllegalArgumentException e) {
                handIsDealt = false;
            }
            assertFalse(handIsDealt);
        }

    }

}
